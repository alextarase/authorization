import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateGroupMenuDto } from './dto/create-group-menu.dto';
import { CreateMenuDto } from './dto/create-menu.dto';
import { GetMenuDto } from './dto/get-menu.dto';
import { GroupMenu } from './entities/group-menu.entity';
import { Menu } from './entities/menu.entity';
import { GroupMenuRepository } from './repositories/group-menu.repository';
import { MenuRepository } from './repositories/menu.repository';

@Injectable()
export class MenuService {
  constructor(
    @InjectRepository(MenuRepository)
    private menuRepository: MenuRepository,
    private groupMenuRepository: GroupMenuRepository,
  ) {}

  async getMenuById(id: number): Promise<Menu> {
    const found = await this.menuRepository.findOne(id);

    if (!found) {
      throw new NotFoundException(`Menu with ID "${id}" not found`);
    }

    return found;
  }

  async getGroupMenuById(id: number): Promise<GroupMenu> {
    const found = await this.groupMenuRepository.findOne(id);

    if (!found) {
      throw new NotFoundException(`GroupMenu with ID "${id}" not found`);
    }

    return found;
  }

  async getGroupMenu(): Promise<GroupMenu[]> {
    return this.groupMenuRepository.getGroupMenu();
  }

  async createGroupMenu(
    createGroupMenuDto: CreateGroupMenuDto,
  ): Promise<GroupMenu> {
    return this.groupMenuRepository.createGroupMenu(createGroupMenuDto);
  }

  async createMenu(createMenuDto: CreateMenuDto): Promise<Menu> {
    const groupMenu = await this.getGroupMenuById(createMenuDto.groupMenu);
    return this.menuRepository.createMenu(createMenuDto, groupMenu);
  }

  async getMenu(getMenuDto: GetMenuDto): Promise<Menu[]> {
    return this.menuRepository.getMenu(getMenuDto);
  }

  async deleteMenuById(id: number): Promise<void> {
    const result = await this.menuRepository.delete(id);

    if (result.affected === 0) {
      throw new NotFoundException(`Menu with ID "${id}" not found`);
    }
  }
}
