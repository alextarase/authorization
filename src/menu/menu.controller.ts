import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Query,
} from '@nestjs/common';
import { CreateGroupMenuDto } from './dto/create-group-menu.dto';
import { CreateMenuDto } from './dto/create-menu.dto';
import { GetMenuDto } from './dto/get-menu.dto';
import { GroupMenu } from './entities/group-menu.entity';
import { Menu } from './entities/menu.entity';
import { MenuService } from './menu.service';

@Controller('menu')
export class MenuController {
  constructor(private menuService: MenuService) {}

  @Post()
  createMenu(@Body() createMenuDto: CreateMenuDto): Promise<Menu> {
    return this.menuService.createMenu(createMenuDto);
  }

  @Get()
  getMenu(@Query() getMenuDto: GetMenuDto): Promise<Menu[]> {
    return this.menuService.getMenu(getMenuDto);
  }

  @Get('groupMenu')
  getMenuGroups(): Promise<GroupMenu[]> {
    return this.menuService.getGroupMenu();
  }

  @Post('createGroupMenu')
  createGroupMenu(
    @Body() createGroupMenuDto: CreateGroupMenuDto,
  ): Promise<GroupMenu> {
    return this.menuService.createGroupMenu(createGroupMenuDto);
  }

  @Delete('/:id')
  deleteMenuById(@Param('id', ParseIntPipe) id: number): Promise<void> {
    return this.menuService.deleteMenuById(id);
  }
}
