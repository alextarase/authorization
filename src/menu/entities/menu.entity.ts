import { OrderContent } from 'src/order/entities/order-content.entity';
import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { GroupMenu } from './group-menu.entity';

@Entity()
export class Menu extends BaseEntity {
  @PrimaryGeneratedColumn()
  @OneToMany(() => OrderContent, (ordercontent) => ordercontent.menuID)
  id: number;

  @Column()
  name: string;

  @Column()
  cost: number;

  @Column()
  colories: number;

  @Column()
  weight: number;

  @Column()
  description: string;

  @ManyToOne(() => GroupMenu, (groupMenu) => groupMenu.dishes)
  @JoinColumn()
  groupMenu: GroupMenu;
}
