import { EntityRepository, Repository } from 'typeorm';
import { CreateMenuDto } from '../dto/create-menu.dto';
import { GetMenuDto } from '../dto/get-menu.dto';
import { Menu } from '../entities/menu.entity';
import { GroupMenu } from './../entities/group-menu.entity';

@EntityRepository(Menu)
export class MenuRepository extends Repository<Menu> {
  async getMenu(getMenuDto: GetMenuDto): Promise<Menu[]> {
    const { groupMenu } = getMenuDto;

    const query = this.createQueryBuilder('menu');

    if (groupMenu) {
      query.andWhere('(menu.groupMenu = :groupMenu)', {
        groupMenu,
      });
    }

    const menu = await query.getMany();
    return menu;
  }

  async createMenu(
    createMenuDto: CreateMenuDto,
    groupMenu: GroupMenu,
  ): Promise<Menu> {
    const { name, cost, colories, weight, description } = createMenuDto;

    const menu = new Menu();
    menu.name = name;
    menu.cost = cost;
    menu.colories = colories;
    menu.weight = weight;
    menu.description = description;
    menu.groupMenu = groupMenu;
    await menu.save();
    return menu;
  }
}
