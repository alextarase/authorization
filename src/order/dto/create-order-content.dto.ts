import { MaxLength } from 'class-validator';

export class CreateOrderContentDto {
  @MaxLength(10)
  number: number;
  Date_time_start_coocking: Date;
  Date_time_order_done: Date;
  Date_time_order_accept: Date;
  Date_time_order_served: Date;
  menuID: number;
  staffID: number;
  orderID: number;
}
