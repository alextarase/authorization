import { OrderStatus } from '../order-status.enum';

export class GetOrderFilterDto {
  status: OrderStatus;
}
