import { IsString, MaxLength, MinLength } from 'class-validator';

export class CreateOrderDto {
  @IsString()
  @MinLength(3)
  @MaxLength(20)
  name: string;
  @MaxLength(10)
  number: number;
  @MaxLength(10)
  payed_summ: number;
  @IsString()
  @MinLength(3)
  @MaxLength(20)
  status: string;
  Date_time_start: Date;
  Date_time_finish: Date;
  staffID: number;
  boardID: number;
  customersID: number;
}
