import { Menu } from 'src/menu/entities/menu.entity';
import { Staff } from 'src/staff/staff.entity';
import { EntityRepository, Repository } from 'typeorm';
import { CreateOrderContentDto } from '../dto/create-order-content.dto';
import { GetOrderContentFilterDto } from '../dto/get-order-content-filter.dto';
import { OrderContent } from '../entities/order-content.entity';
import { Order } from '../entities/order.entity';

@EntityRepository(OrderContent)
export class OrderContentRepository extends Repository<OrderContent> {
  async getOrderContent(
    getOrderContentFilterDto: GetOrderContentFilterDto,
  ): Promise<OrderContent[]> {
    const query = this.createQueryBuilder('order_content');
    const { orderID } = getOrderContentFilterDto;

    if (orderID) {
      query.andWhere('order_content.orderID = :orderID', {
        orderID,
      });
    }
    const orderContent = await query.getMany();
    return orderContent;
  }

  async createOrderContent(
    createOrderContentDto: CreateOrderContentDto,
    menu: Menu,
    staff: Staff,
    order: Order,
  ): Promise<OrderContent> {
    const {
      number,
      Date_time_start_coocking,
      Date_time_order_done,
      Date_time_order_accept,
      Date_time_order_served,
    } = createOrderContentDto;
    const orderContent = new OrderContent();
    orderContent.number = number;
    orderContent.Date_time_start_coocking = Date_time_start_coocking;
    orderContent.Date_time_order_done = Date_time_order_done;
    orderContent.Date_time_order_accept = Date_time_order_accept;
    orderContent.Date_time_order_served = Date_time_order_served;
    orderContent.menuID = menu;
    orderContent.staffID = staff;
    orderContent.orderID = order;
    await orderContent.save();
    return orderContent;
  }
}
