import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { Board } from 'src/board/board.entity';
import { BoardRepository } from 'src/board/board.repository';
import { Customers } from 'src/customers/customers.entity';
import { CustomersRepository } from 'src/customers/customers.repository';
import { Staff } from 'src/staff/staff.entity';
import { StaffRepository } from 'src/staff/staff.repository';

import { CreateOrderContentDto } from './dto/create-order-content.dto';
import { CreateOrderDto } from './dto/create-order.dto';
import { GetOrderContentFilterDto } from './dto/get-order-content-filter.dto';
import { GetOrderFilterDto } from './dto/get-order-filter.dto';
import { OrderContent } from './entities/order-content.entity';
import { Order } from './entities/order.entity';
import { OrderStatus } from './order-status.enum';
import { OrderContentRepository } from './repositories/order-content.repository';

import { OrderRepository } from './repositories/order.repository';
import { MenuRepository } from './../menu/repositories/menu.repository';
import { Menu } from 'src/menu/entities/menu.entity';

@Injectable()
export class OrderService {
  constructor(
    @InjectRepository(OrderRepository)
    private orderRepository: OrderRepository,
    private staffRepository: StaffRepository,
    private orderContentRepository: OrderContentRepository,
    private boardRepository: BoardRepository,
    private customersRepository: CustomersRepository,
    private menuRepository: MenuRepository,
  ) {}

  async getStaffById(id: number): Promise<Staff> {
    const found = await this.staffRepository.findOne(id);

    if (!found) {
      throw new NotFoundException(`Staff with ID "${id}" not found`);
    }

    return found;
  }

  async getMenuById(id: number): Promise<Menu> {
    const found = await this.menuRepository.findOne(id);

    if (!found) {
      throw new NotFoundException(`Menu with ID "${id}" not found`);
    }

    return found;
  }

  async getOrderById(id: number): Promise<Order> {
    const found = await this.orderRepository.findOne(id);

    if (!found) {
      throw new NotFoundException(`Order with ID "${id}" not found`);
    }

    return found;
  }

  async getBoardById(id: number): Promise<Board> {
    const found = await this.boardRepository.findOne(id);

    if (!found) {
      throw new NotFoundException(`Board with ID "${id}" not found`);
    }

    return found;
  }

  async getCustomerById(id: number): Promise<Customers> {
    const found = await this.customersRepository.findOne(id);

    if (!found) {
      throw new NotFoundException(`Customer with ID "${id}" not found`);
    }

    return found;
  }

  async createOrder(createOrderDto: CreateOrderDto): Promise<Order> {
    const staff = await this.getStaffById(createOrderDto.staffID);
    const board = await this.getBoardById(createOrderDto.boardID);
    const customer = await this.getCustomerById(createOrderDto.customersID);
    return this.orderRepository.createOrder(
      createOrderDto,
      staff,
      board,
      customer,
    );
  }

  async getOrder(filterDto: GetOrderFilterDto): Promise<Order[]> {
    return this.orderRepository.getOrder(filterDto);
  }

  async deleteOrderById(id: number): Promise<void> {
    const result = await this.orderRepository.delete(id);

    if (result.affected === 0) {
      throw new NotFoundException(`Order with ID "${id}" not found`);
    }
  }

  async updateOrderStatus(id: number, status: OrderStatus): Promise<Order> {
    const order = await this.getOrderById(id);
    order.status = status;
    await order.save();
    return order;
  }

  async getOrdercontent(
    getOrderContentFilterDto: GetOrderContentFilterDto,
  ): Promise<OrderContent[]> {
    return this.orderContentRepository.getOrderContent(
      getOrderContentFilterDto,
    );
  }

  async createOrderContent(
    createOrderContentDto: CreateOrderContentDto,
  ): Promise<OrderContent> {
    const staff = await this.getStaffById(createOrderContentDto.staffID);
    const order = await this.getOrderById(createOrderContentDto.orderID);
    const menu = await this.getMenuById(createOrderContentDto.menuID);
    return this.orderContentRepository.createOrderContent(
      createOrderContentDto,
      menu,
      staff,
      order,
    );
  }
}
