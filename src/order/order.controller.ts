import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { CreateOrderContentDto } from './dto/create-order-content.dto';
import { CreateOrderDto } from './dto/create-order.dto';
import { GetOrderContentFilterDto } from './dto/get-order-content-filter.dto';
import { GetOrderFilterDto } from './dto/get-order-filter.dto';
import { OrderContent } from './entities/order-content.entity';
import { Order } from './entities/order.entity';
import { OrderStatusValidationPipe } from './order-status-validation.pipe';
import { OrderStatus } from './order-status.enum';

import { OrderService } from './order.service';

@Controller('order')
export class OrderController {
  constructor(private orderService: OrderService) {}

  @Post('orderContent')
  createOrderContent(
    @Body() createOrderContentDto: CreateOrderContentDto,
  ): Promise<OrderContent> {
    return this.orderService.createOrderContent(createOrderContentDto);
  }

  @Get('orderContent')
  getOrderContent(
    @Query() getOrderContentFilterDto: GetOrderContentFilterDto,
  ): Promise<OrderContent[]> {
    return this.orderService.getOrdercontent(getOrderContentFilterDto);
  }

  @Post()
  createOrder(@Body() createOrderDto: CreateOrderDto): Promise<Order> {
    return this.orderService.createOrder(createOrderDto);
  }

  @Get()
  getOrder(@Query() filterDto: GetOrderFilterDto): Promise<Order[]> {
    return this.orderService.getOrder(filterDto);
  }

  @Get('/:id')
  getOrderById(@Param('id', ParseIntPipe) id: number): Promise<Order> {
    return this.orderService.getOrderById(id);
  }

  @Delete('/:id')
  deleteOrderByid(@Param('id', ParseIntPipe) id: number): Promise<void> {
    return this.orderService.deleteOrderById(id);
  }

  @Patch('/:id/status')
  updateStaffStatus(
    @Param('id', ParseIntPipe) id: number,
    @Body('status', OrderStatusValidationPipe) status: OrderStatus,
  ): Promise<Order> {
    return this.orderService.updateOrderStatus(id, status);
  }
}
