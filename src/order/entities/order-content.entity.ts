import { Menu } from 'src/menu/entities/menu.entity';
import { Staff } from 'src/staff/staff.entity';
import { Order } from './order.entity';

import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class OrderContent extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  number: number;

  @Column({ type: 'timestamptz' })
  Date_time_start_coocking: Date;

  @Column({ type: 'timestamptz' })
  Date_time_order_done: Date;

  @Column({ type: 'timestamptz' })
  Date_time_order_accept: Date;

  @Column({ type: 'timestamptz' })
  Date_time_order_served: Date;

  @ManyToOne(() => Menu, (menu) => menu.id)
  @JoinColumn()
  menuID: Menu;

  @ManyToOne(() => Staff, (staff) => staff.id)
  @JoinColumn()
  staffID: Staff;

  @ManyToOne(() => Order, (order) => order.id)
  orderID: Order;
}
