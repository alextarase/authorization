import { Board } from 'src/board/board.entity';
import { Customers } from 'src/customers/customers.entity';
import { Staff } from 'src/staff/staff.entity';
import { OrderContent } from './order-content.entity';

import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToMany,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class Order extends BaseEntity {
  @PrimaryGeneratedColumn()
  // @OneToOne(() => OrderContent, (ordercontent) => ordercontent.orderID)
  id: number;

  @Column()
  name: string;

  @Column()
  number: number;

  @Column()
  payed_summ: number;

  @Column()
  status: string;

  @Column({ type: 'timestamptz' })
  Date_time_start: Date;

  @Column({ type: 'timestamptz' })
  Date_time_finish: Date;

  @OneToOne(() => Board, (board) => board.id)
  @JoinColumn()
  boardID: Board;

  @ManyToOne(() => Staff, (staff) => staff.order)
  @JoinColumn()
  staffID: Staff;

  @OneToOne(() => Customers, (customers) => customers.id)
  @JoinColumn()
  customersID: Customers;

  @OneToMany(() => OrderContent, (ordercontent) => ordercontent.orderID)
  @JoinColumn()
  ordercontent: OrderContent;
}
