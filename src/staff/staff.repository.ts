import { User } from 'src/auth/user.entity';
import { EntityRepository, Repository } from 'typeorm';
import { CreateStaffDto } from './dto/create-staff.dto';
import { GetStaffFilterDto } from './dto/get-staff-filter.dto';
import { StaffPosition } from './staff-position.enum';
import { Staff } from './staff.entity';

@EntityRepository(Staff)
export class StaffRepository extends Repository<Staff> {
  async getStaff(filterDto: GetStaffFilterDto): Promise<Staff[]> {
    const { position, search } = filterDto;
    const query = this.createQueryBuilder('staff');

    if (position) {
      query.andWhere('staff.position = :position', { position });
    }

    if (search) {
      query.andWhere(
        '(staff.name LIKE :search OR staff.phone_number LIKE :search)',
        { search: `%${search}%` },
      );
    }

    const staff = await query.getMany();
    return staff;
  }

  async createStaff(
    createStaffDto: CreateStaffDto,
    user: User,
  ): Promise<Staff> {
    const { name, salary, phone_number } = createStaffDto;

    const staff = new Staff();
    staff.name = name;
    staff.salary = salary;
    staff.position = StaffPosition.Waiter;
    staff.phone_number = phone_number;
    staff.user = user;
    await staff.save();

    delete staff.user;

    return staff;
  }
}
