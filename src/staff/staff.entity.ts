import { User } from 'src/auth/user.entity';
import { OrderContent } from 'src/order/entities/order-content.entity';
import { Order } from 'src/order/entities/order.entity';
import {
  BaseEntity,
  Column,
  Entity,
  JoinColumn,
  ManyToOne,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { StaffPosition } from './staff-position.enum';

@Entity()
export class Staff extends BaseEntity {
  @PrimaryGeneratedColumn()
  // @OneToOne(() => Order, (order) => order.staffID)
  id: number;

  @Column()
  name: string;

  @Column()
  salary: number;

  @Column()
  position: StaffPosition;

  @Column()
  phone_number: string;

  @ManyToOne(() => User, (user) => user.staff)
  user: User;

  @OneToMany(() => Order, (order) => order.staffID)
  order: Order;

  @OneToMany(() => OrderContent, (orderContent) => orderContent.staffID)
  orderContent: OrderContent;
}
