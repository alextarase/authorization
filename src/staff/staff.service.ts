import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateStaffDto } from './dto/create-staff.dto';
import { StaffRepository } from './staff.repository';
import { GetStaffFilterDto } from './dto/get-staff-filter.dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Staff } from './staff.entity';
import { StaffPosition } from './staff-position.enum';
import { User } from 'src/auth/user.entity';

@Injectable()
export class StaffService {
  constructor(
    @InjectRepository(StaffRepository)
    private staffRepository: StaffRepository,
  ) {}

  async getStaffById(id: number): Promise<Staff> {
    const found = await this.staffRepository.findOne(id);

    if (!found) {
      throw new NotFoundException(`Staff with ID "${id}" not found`);
    }

    return found;
  }

  async getStaff(filterDto: GetStaffFilterDto): Promise<Staff[]> {
    return this.staffRepository.getStaff(filterDto);
  }

  async deleteStaffById(id: number): Promise<void> {
    const result = await this.staffRepository.delete(id);

    if (result.affected === 0) {
      throw new NotFoundException(`Staff with ID "${id}" not found`);
    }
  }
  async createStaff(
    createStaffDto: CreateStaffDto,
    user: User,
  ): Promise<Staff> {
    return this.staffRepository.createStaff(createStaffDto, user);
  }

  async updateStaffPosition(
    id: number,
    position: StaffPosition,
  ): Promise<Staff> {
    const staff = await this.getStaffById(id);
    staff.position = position;
    await staff.save();
    return staff;
  }

  async updateStaffSalary(id: number, salary: number): Promise<Staff> {
    const staff = await this.getStaffById(id);
    staff.salary = salary;
    await staff.save();
    return staff;
  }
}
