import {
  Body,
  Controller,
  Delete,
  Get,
  Logger,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
  UseGuards,
  UsePipes,
  ValidationPipe,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from 'src/auth/get-user.decorator';
import { User } from 'src/auth/user.entity';
import { CreateStaffDto } from './dto/create-staff.dto';
import { GetStaffFilterDto } from './dto/get-staff-filter.dto';
import { StaffPositionValidationPipe } from './staff-position-validation.pipe';
import { StaffPosition } from './staff-position.enum';
import { Staff } from './staff.entity';

import { StaffService } from './staff.service';

@Controller('staff')
export class StaffController {
  private logger = new Logger('StaffController');
  constructor(private staffService: StaffService) {}

  @Get()
  getStaff(@Query() filterDto: GetStaffFilterDto): Promise<Staff[]> {
    return this.staffService.getStaff(filterDto);
  }

  @Post()
  @UseGuards(AuthGuard())
  @UsePipes(ValidationPipe)
  createStaf(
    @Body() createStaffDto: CreateStaffDto,
    @GetUser() user: User,
  ): Promise<Staff> {
    this.logger.verbose(
      `User "${user.username}" creating a new staff. Data: ${JSON.stringify(
        createStaffDto,
      )}`,
    );
    return this.staffService.createStaff(createStaffDto, user);
  }

  @Get('/:id')
  getStaffById(@Param('id', ParseIntPipe) id: number): Promise<Staff> {
    return this.staffService.getStaffById(id);
  }

  @Delete('/:id')
  deleteStaffByid(@Param('id', ParseIntPipe) id: number): Promise<void> {
    return this.staffService.deleteStaffById(id);
  }

  @Patch('/:id/position')
  updateStaffPosition(
    @Param('id', ParseIntPipe) id: number,
    @Body('position', StaffPositionValidationPipe) position: StaffPosition,
  ): Promise<Staff> {
    return this.staffService.updateStaffPosition(id, position);
  }

  @Patch('/:id/salary')
  updateStaffSalary(
    @Param('id', ParseIntPipe) id: number,
    @Body('salary', ParseIntPipe) salary: number,
  ): Promise<Staff> {
    return this.staffService.updateStaffSalary(id, salary);
  }
}
