export enum StaffPosition {
  Waiter = 'Waiter',
  Manager = 'Manager',
  Admin = 'Admin',
  Boss = 'Boss',
}
