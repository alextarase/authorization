import { BadRequestException, PipeTransform } from '@nestjs/common';
import { StaffPosition } from './staff-position.enum';

export class StaffPositionValidationPipe implements PipeTransform {
  readonly allowedPositions = [
    StaffPosition.Waiter,
    StaffPosition.Manager,
    StaffPosition.Admin,
    StaffPosition.Boss,
  ];

  transform(value: any) {
    if (!this.isPositionValid(value)) {
      throw new BadRequestException(`"${value}" is an invalid position`);
    }

    return value;
  }

  private isPositionValid(position: any) {
    const idx = this.allowedPositions.indexOf(position);
    return idx !== -1;
  }
}
