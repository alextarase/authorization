import { StaffPosition } from '../staff-position.enum';

export class GetStaffFilterDto {
  position: StaffPosition;
  search: string;
}
