export class CreateCustomerDto {
  name: string;
  bonus_programm: number;
  phone_number: string;
  bonus_card_number: string;
}
