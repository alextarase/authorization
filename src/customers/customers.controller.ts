import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Query,
} from '@nestjs/common';
import { Customers } from './customers.entity';
import { CustomersService } from './customers.service';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { GetCustomerDto } from './dto/get-customer.dto';

@Controller('customers')
export class CustomersController {
  constructor(private customersService: CustomersService) {}

  @Post()
  createCustomer(
    @Body() createCustomerDto: CreateCustomerDto,
  ): Promise<Customers> {
    return this.customersService.createCustomer(createCustomerDto);
  }

  @Get()
  getCustomers(@Query() getCustomerDto: GetCustomerDto): Promise<Customers[]> {
    return this.customersService.getCustomers(getCustomerDto);
  }

  @Delete('/:id')
  deleteCustomerById(@Param('id', ParseIntPipe) id: number): Promise<void> {
    return this.customersService.deleteCustomerById(id);
  }
}
