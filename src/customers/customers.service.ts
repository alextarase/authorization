import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Customers } from './customers.entity';
import { CustomersRepository } from './customers.repository';
import { CreateCustomerDto } from './dto/create-customer.dto';
import { GetCustomerDto } from './dto/get-customer.dto';

@Injectable()
export class CustomersService {
  constructor(
    @InjectRepository(CustomersRepository)
    private customersRepositiry: CustomersRepository,
  ) {}

  async createCustomer(
    createCustomerDto: CreateCustomerDto,
  ): Promise<Customers> {
    return this.customersRepositiry.createCustomer(createCustomerDto);
  }

  async getCustomers(getCustomerDto: GetCustomerDto): Promise<Customers[]> {
    return this.customersRepositiry.getCustomer(getCustomerDto);
  }

  async deleteCustomerById(id: number): Promise<void> {
    const result = await this.customersRepositiry.delete(id);

    if (result.affected === 0) {
      throw new NotFoundException(`Customer with ID "${id}" not found`);
    }
  }
}
