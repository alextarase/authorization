import { Order } from 'src/order/entities/order.entity';
import {
  BaseEntity,
  Column,
  Entity,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import { BoardOrder } from './board-order.entity';

@Entity()
export class Board extends BaseEntity {
  @PrimaryGeneratedColumn()
  @OneToOne(() => Order, (order) => order.boardID)
  @OneToMany(() => BoardOrder, (boardorder) => boardorder.boardID)
  id: number;

  // @OneToOne(() => BoardOrder, (boardorder) => boardorder.board_order_ID)
  // @JoinColumn()
  // board_order_ID: number;

  @Column()
  number: number;

  @Column()
  status: string;

  @Column()
  sit_number: number;
}
