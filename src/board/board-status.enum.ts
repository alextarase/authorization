export enum BoardStatus {
  FREE = 'FREE',
  ORDERED = 'ORDERED',
  BUSY = 'BUSY',
}
