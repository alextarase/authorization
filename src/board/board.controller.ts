import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Patch,
  Post,
  Query,
} from '@nestjs/common';
import { BoardOrder } from './board-order.entity';
import { BoardStatusValidationPipe } from './board-status-validation.pipe';
import { BoardStatus } from './board-status.enum';
import { Board } from './board.entity';
import { BoardService } from './board.service';
import { CreateBoardOrderDto } from './dto/create-board-order.dto';
import { CreateBoardDto } from './dto/create-board.dto';
import { GetBoardFilterDto } from './dto/get-board-filter.dto';
import { GetBoardOrderFilterDto } from './dto/get-borderOrder.filter.dto';

@Controller('board')
export class BoardController {
  constructor(private boardService: BoardService) {}

  @Post()
  createBoard(@Body() createBoardDto: CreateBoardDto): Promise<Board> {
    return this.boardService.createBoard(createBoardDto);
  }

  @Post('boardOrder')
  createBoardOrder(
    @Body() createBoardOrderDto: CreateBoardOrderDto,
  ): Promise<BoardOrder> {
    return this.boardService.createBoardOrder(createBoardOrderDto);
  }

  @Get()
  getBoard(@Query() filterDto: GetBoardFilterDto): Promise<Board[]> {
    return this.boardService.getBoard(filterDto);
  }

  @Get('boardOrder')
  getBoardOrder(
    @Query() filterDto: GetBoardOrderFilterDto,
  ): Promise<BoardOrder[]> {
    return this.boardService.getBoardOrder(filterDto);
  }

  @Get('/:id')
  getBoardById(@Param('id', ParseIntPipe) id: number): Promise<Board> {
    return this.boardService.getBoardById(id);
  }

  @Delete('/:id')
  deleteBoardByid(@Param('id', ParseIntPipe) id: number): Promise<void> {
    return this.boardService.deleteOrderById(id);
  }

  @Patch('/:id')
  updateBoardStatus(
    @Param('id', ParseIntPipe) id: number,
    @Body('status', BoardStatusValidationPipe) status: BoardStatus,
  ): Promise<Board> {
    return this.boardService.updateBoardStatus(id, status);
  }
}
