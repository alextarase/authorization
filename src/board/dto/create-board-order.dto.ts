import { BoardStatus } from '../board-status.enum';

export class CreateBoardOrderDto {
  Date_time_ordered: Date;
  name: string;
  phone_number: string;
  sit_number: number;
  boardID: number;
  board_status: BoardStatus;
}
