import { BoardStatus } from '../board-status.enum';

export class GetBoardFilterDto {
  status = BoardStatus;
}
