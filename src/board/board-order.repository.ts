import { EntityRepository, Repository } from 'typeorm';
import { BoardOrder } from './board-order.entity';
import { BoardStatus } from './board-status.enum';
import { Board } from './board.entity';
import { CreateBoardOrderDto } from './dto/create-board-order.dto';
import { GetBoardOrderFilterDto } from './dto/get-borderOrder.filter.dto';

@EntityRepository(BoardOrder)
export class BoardOrderRepository extends Repository<BoardOrder> {
  async getBoardOrder(
    filterDto: GetBoardOrderFilterDto,
  ): Promise<BoardOrder[]> {
    const { boardID } = filterDto;
    const query = this.createQueryBuilder('boardOrder');

    if (boardID) {
      query.andWhere('boardOrder.boardID = :boardID', {
        boardID,
      });
    }

    const boardOrder = await query.getMany();
    return boardOrder;
  }

  async createBoardOrder(
    createBoardOrderDto: CreateBoardOrderDto,
    board: Board,
  ): Promise<BoardOrder> {
    const { Date_time_ordered, name, phone_number, sit_number } =
      createBoardOrderDto;

    const boardOrder = new BoardOrder();
    boardOrder.Date_time_ordered = Date_time_ordered;
    boardOrder.name = name;
    boardOrder.phone_number = phone_number;
    boardOrder.sit_number = sit_number;
    boardOrder.boardID = board;
    await boardOrder.save();
    return boardOrder;
  }
}
