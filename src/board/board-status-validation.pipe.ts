import { BadRequestException, PipeTransform } from '@nestjs/common';
import { BoardStatus } from './board-status.enum';

export class BoardStatusValidationPipe implements PipeTransform {
  readonly allowedStatus = [
    BoardStatus.FREE,
    BoardStatus.ORDERED,
    BoardStatus.BUSY,
  ];

  transform(value: any) {
    if (!this.isStatusValid(value)) {
      throw new BadRequestException(`"${value}" is an invalid status`);
    }

    return value;
  }

  private isStatusValid(status: any) {
    const idx = this.allowedStatus.indexOf(status);
    return idx !== -1;
  }
}
