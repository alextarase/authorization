import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BoardOrderRepository } from './board-order.repository';
import { BoardController } from './board.controller';
import { BoardRepository } from './board.repository';
import { BoardService } from './board.service';

@Module({
  imports: [TypeOrmModule.forFeature([BoardRepository, BoardOrderRepository])],
  controllers: [BoardController],
  providers: [BoardService],
})
export class BoardModule {}
