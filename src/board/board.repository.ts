import { EntityRepository, Repository } from 'typeorm';
import { BoardStatus } from './board-status.enum';
import { Board } from './board.entity';
import { CreateBoardDto } from './dto/create-board.dto';
import { GetBoardFilterDto } from './dto/get-board-filter.dto';

@EntityRepository(Board)
export class BoardRepository extends Repository<Board> {
  async getBoard(filterDto: GetBoardFilterDto): Promise<Board[]> {
    const { status } = filterDto;
    const query = this.createQueryBuilder('board');

    if (status) {
      query.andWhere('board.status = :status', { status });
    }

    const board = await query.getMany();
    return board;
  }

  async createBoard(createBoardDto: CreateBoardDto): Promise<Board> {
    const { number, sit_number } = createBoardDto;

    const board = new Board();
    board.number = number;
    board.status = BoardStatus.FREE;
    board.sit_number = sit_number;
    await board.save();
    return board;
  }
}
